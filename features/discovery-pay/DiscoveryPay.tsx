import React from 'react';
import { Button, Text, View } from 'react-native';

export default class DiscoveryPay extends React.Component {

	static navigationOptions = {

		headerStyle: {
			backgroundColor: '#f4511e',
		},
		headerTintColor: '#fff',
		headerTitleStyle: {
			fontWeight: 'bold',
		},
		headerTitle: <Text>Discovery Pay</Text>,
	};

	render() {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Text>Discovery Pay</Text>
			</View>
		);
	}
}
