enum TransferRoute {
	TRANSFER = 'Transfer',
	SCHEDULE = 'Transfer Schedule',
	REVIEW = 'Transfer Review',
	RESPONSE = 'Transfer Response'
}

export default TransferRoute;
