import { NavigationRouteConfigMap } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import Transfer from 'discovery-bank/features/transfers/Transfer';
import TransferResponse from 'discovery-bank/features/transfers/TransferResponse';
import TransferReview from 'discovery-bank/features/transfers/TransferReview';
import TransferRoute from 'discovery-bank/features/transfers/TransferRoutes.enum';
import TransferSchedule from 'discovery-bank/features/transfers/TransferSchedule';
import { NavigationStackConfig } from 'react-navigation-stack/lib/typescript/types';

const TRANSFER_ROUTE_CONFIG: NavigationRouteConfigMap = {
	[TransferRoute.TRANSFER]: {screen: Transfer},
	[TransferRoute.SCHEDULE]: {screen: TransferSchedule},
	[TransferRoute.REVIEW]: {screen: TransferReview},
	[TransferRoute.RESPONSE]: {screen: TransferResponse}
};

const TRANFER_STACK_CONFIG: NavigationStackConfig & any = {
	initialRouteName: TransferRoute.TRANSFER,
	headerMode: 'none'
};

const TransferStackNavigator = createStackNavigator(TRANSFER_ROUTE_CONFIG, TRANFER_STACK_CONFIG);

export default TransferStackNavigator;
