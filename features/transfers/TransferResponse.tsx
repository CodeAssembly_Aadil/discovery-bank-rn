import React, { Component } from 'react';

import { Text, View } from 'react-native';

import { ApplicationStateStore } from 'discovery-bank/core/ApplicationState.store';
import { withNavigationFocus } from 'react-navigation';

class TransferResponse extends Component {
	private focusListener: any;

	componentDidMount() {
		const {navigation} = this.props;
		this.focusListener = navigation.addListener('didFocus', this.screenInFocus);
	}

	render() {
		return (
			<View style={{flex: 1, alignItems: 'center', padding: 20}}>
				<Text>Transfer Response</Text>
			</View>
		);
	}
	componentWillUnmount() {
		this.focusListener.remove();
	}

	private screenInFocus = (): void => {
		ApplicationStateStore.setAppHeaderTitle('Transfer Response');
		ApplicationStateStore.hideBackButton();
	}
}

export default withNavigationFocus(TransferResponse);
