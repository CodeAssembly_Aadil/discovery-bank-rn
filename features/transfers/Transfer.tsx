import React, { Component } from 'react';

import { Button, Content, Form, Input, Item, Label, Text } from 'native-base';

import { withNavigationFocus } from 'react-navigation';

import { ApplicationStateStore } from 'discovery-bank/core/ApplicationState.store';
import AccountCard from 'discovery-bank/core/ui/AccountCard';
import ExpandingList from 'discovery-bank/core/ui/ExpandingList';
import { MockAccounts } from 'discovery-bank/features/transfers/MockAccounts';
import TransferRoute from 'discovery-bank/features/transfers/TransferRoutes.enum';
import Account from 'discovery-bank/models/Account.model';
import NavigationService from 'discovery-bank/navigation/NavigationService';

class Transfer extends Component {
	private focusListener: any;

	componentDidMount() {
		const {navigation} = this.props;
		this.focusListener = navigation.addListener('didFocus', this.screenInFocus);
	}

	render() {
		return (
			<Content
				style={{flex: 1}}
				contentContainerStyle={{
					padding: 20,
					minHeight: '100%'
				}}
			>
				<Text style={{marginBottom: 8}}>From</Text>

				<ExpandingList
					style={{marginBottom: 16, borderRadius: 3}}
					placeholder={<AccountCard account={MockAccounts[0]} />}
					data={MockAccounts}
					keyExtractor={(account: Account) => account.number}
					renderListItem={({item}: {item: Account; index: number; separators: any}) => (
						<AccountCard account={item} key={item.number} />
					)}
				/>

				<Text style={{marginBottom: 8}}>To</Text>

				<ExpandingList
					style={{marginBottom: 16, borderRadius: 3}}
					placeholder={<AccountCard account={MockAccounts[0]} />}
					data={MockAccounts}
					keyExtractor={(account: Account) => account.number}
					renderListItem={({item}: {item: Account; index: number; separators: any}) => (
						<AccountCard
							style={{
								borderBottomColor: 'lightgrey',
								borderBottomWidth: 1
							}}
							account={item}
						/>
					)}
				/>

				<Form style={{marginBottom: 32}}>
					<Item floatingLabel>
						<Label>Amount</Label>
						<Input />
					</Item>
					<Item floatingLabel>
						<Label>Reference</Label>
						<Input maxLength={30} />
					</Item>
				</Form>

				<Button
					style={{
						alignSelf: 'center',
						backgroundColor: 'navy',
						marginTop: 'auto',
						paddingHorizontal: 40,
						borderRadius: 0,
						height: 32
					}}
					onPress={this.reviewTransferDetails}
				>
					<Text style={{fontSize: 14}}>Next</Text>
				</Button>
			</Content>
		);
	}

	componentWillUnmount() {
		this.focusListener.remove();
	}

	private reviewTransferDetails = () => {
		NavigationService.navigate(TransferRoute.REVIEW);
	}

	private screenInFocus = (): void => {
		ApplicationStateStore.setAppHeaderTitle('Transfer');
		ApplicationStateStore.hideBackButton();
	}
}

export default withNavigationFocus(Transfer);
