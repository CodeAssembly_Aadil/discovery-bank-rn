import { AccountCardType } from 'discovery-bank/core/constants/Account.enum';
import { CurrencySymbol } from 'discovery-bank/core/constants/Currency';
import Account from 'discovery-bank/models/Account.model';

export const MockAccounts: Account[] = [
	{
		name: 'Discovery Black Credit Card',
		number: '4000300020001001',
		productType: 'Credit Card',
		ccy: CurrencySymbol.ZAR,
		balance: 100000,
		type: AccountCardType.BLACK
	},
	{
		name: 'Discovery Gold Debit Card',
		number: '4000300020001002',
		productType: 'Debit Card',
		ccy: CurrencySymbol.ZAR,
		balance: 100000,
		type: AccountCardType.GOLD
	},
	{
		name: 'Discovery Platinum Credit Card',
		number: '4000300020001003',
		productType: 'Credit Card',
		ccy: CurrencySymbol.ZAR,
		balance: 100000,
		type: AccountCardType.PLATINUM
	},
	{
		name: 'Discovery Purple Credit Card',
		number: '4000300020001004',
		productType: 'Credit Card',
		ccy: CurrencySymbol.USD,
		balance: 100000,
		type: AccountCardType.PURPLE
	},
	{
		name: 'Discovery Bank Card',
		number: '4000300020001005',
		productType: 'Debit Card',
		ccy: CurrencySymbol.GBP,
		balance: 100000,
		type: AccountCardType.BANK
	},
	{
		name: 'Discovery Vitaily Card',
		number: '4000300020001006',
		productType: 'Locyalty Card',
		ccy: CurrencySymbol.ZAR,
		balance: 4000,
		type: AccountCardType.VITALITY
	}
];
