import React, { Component } from 'react';

import { Text, View } from 'react-native';

import { withNavigationFocus } from 'react-navigation';

import { ApplicationStateStore } from 'discovery-bank/core/ApplicationState.store';

class TransferReview extends Component {

	private focusListener: any;

	componentDidMount() {
		const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', this.screenInFocus);
	}

	render() {
		return (
			<View style={{ flex: 1, alignItems: 'center', padding: 20 }}>
				<Text>Transfer Review</Text>
			</View>
		);
	}
	componentWillUnmount() {
		this.focusListener.remove();
	}

	private screenInFocus = (): void => {
		ApplicationStateStore.setAppHeaderTitle('Transfer Review');
		ApplicationStateStore.showBackButton();
		ApplicationStateStore.showActionSheetButton();
	}
}

export default withNavigationFocus(TransferReview);
