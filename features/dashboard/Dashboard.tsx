import React, { Component } from 'react';

import { Text, View } from 'react-native';

import { ApplicationStateStore } from 'discovery-bank/core/ApplicationState.store';
import { withNavigationFocus } from 'react-navigation';

class Dashboard extends Component {
	private focusListener;

	componentDidMount() {
		const {navigation} = this.props;
		this.focusListener = navigation.addListener('didFocus', this.screenInFocus);
	}

	render() {
		return (
			<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
				<Text>Dashboard</Text>
			</View>
		);
	}

	componentWillUnmount() {
		this.focusListener.remove();
	}

	private screenInFocus = (): void => {
		ApplicationStateStore.setAppHeaderTitle('Home');
		ApplicationStateStore.hideBackButton();
	}
}

export default withNavigationFocus(Dashboard);
