import React from 'react';

import Route from 'discovery-bank/navigation/Routes';
import { AsyncStorage, Button, Text, View } from 'react-native';

export default class SignIn extends React.Component {
	static navigationOptions = {
		title: 'Please sign in',
	};

	render() {
		return (
			<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
				<Text>Sign In</Text>
				<Button title="Sign in!" onPress={this.signInAsync} />
			</View>
		);
	}

	private signInAsync = async () => {
		await AsyncStorage.setItem('userToken', 'dummy-token');
		this.props.navigation.navigate(Route.APP);
	}
}
