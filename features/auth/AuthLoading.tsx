import React from 'react';

import { ActivityIndicator, AsyncStorage, StatusBar, View } from 'react-native';

import Route from 'discovery-bank/navigation/Routes';

export default class AuthLoading extends React.Component {
	constructor(props: any) {
		super(props);
		this.bootstrapAsync();
	}

	// Fetch the token from storage then navigate to our appropriate place
	private bootstrapAsync = async () => {
		const userToken = await AsyncStorage.getItem('userToken');

		// This will switch to the App screen or Auth screen and this loading
		// screen will be unmounted and thrown away.
		this.props.navigation.navigate(userToken ? Route.APP : Route.AUTH);
	}

	// Render any loading content that you like here
	render() {
		return (
			<View>
				<ActivityIndicator />
				<StatusBar barStyle="default" />
			</View>
		);
	}
}
