import React from 'react';

import { Button, FooterTab, Text, } from 'native-base';
import { TextStyle, View } from 'react-native';

import { AntDesign, Feather } from '@expo/vector-icons';

import { DiscoveryGradient } from 'discovery-bank/core/ui/DiscoveryGradient';
import NavigationService from 'discovery-bank/navigation/NavigationService';
import Route from 'discovery-bank/navigation/Routes';

const activeTintColor = 'tomato';
const inactiveTintColor = 'gray';
const textStyle: TextStyle = {
	textAlign: 'center',
	fontSize: 10,
};

const BottomTabs = () => {
	return (
		<View style={{ flex: 1, alignItems: "center", alignContent: "center" }} >
			<DiscoveryGradient style={{ height: 1, width: '100%' }} />

			<FooterTab style={{ paddingBottom: 10 }}>

				<Button onPress={() => NavigationService.navigate(Route.HOME)}>
					<AntDesign name="home" size={24} color={inactiveTintColor} />
					<Text style={textStyle}>Home</Text>
				</Button>

				<Button onPress={() => NavigationService.navigate(Route.ACCOUNTS)}>
					<Feather name="credit-card" size={24} color={inactiveTintColor} />
					<Text style={textStyle}>Accounts</Text>
				</Button>

				<Button onPress={() => NavigationService.navigate(Route.PAY)}>
					<Feather name="plus-circle" size={24} color={inactiveTintColor} />
					<Text style={textStyle}>Pay</Text>
				</Button>

				<Button onPress={() => NavigationService.navigate(Route.TRANSFERS)}>
					<AntDesign name="swap" size={24} color={inactiveTintColor} />
					<Text style={textStyle}>Transfers</Text>
				</Button>

				<Button onPress={() => NavigationService.openRightDrawer()}>
					<Feather name="more-horizontal" size={24} color={inactiveTintColor} />
					<Text style={textStyle}>More</Text>
				</Button>
			</FooterTab>
		</View>
	);
};

export default BottomTabs;
