import { NavigationRouteConfigMap } from 'react-navigation';

import Dashboard from 'discovery-bank/features/dashboard/Dashboard';
import DiscoveryPay from 'discovery-bank/features/discovery-pay/DiscoveryPay';
import DynamicCreditFacility from 'discovery-bank/features/dynamic-credit-facility/DynamicCreditFacility';
import NotFound from 'discovery-bank/features/not-found/NotFound';
import TransferStackNavigator from 'discovery-bank/features/transfers/TransferStackNavigator';
import Route from 'discovery-bank/navigation/Routes';

const APP_ROUTE_CONFIG: NavigationRouteConfigMap = {
	// Bottom Tab Routes
	[Route.HOME]: {screen: Dashboard},
	[Route.ACCOUNTS]: {screen: NotFound},
	[Route.PAY]: {screen: NotFound},
	[Route.TRANSFERS]: {screen: TransferStackNavigator},

	// Right Drawer Routes
	[Route.DYNAMIC_CREDIT_FACILITY]: {screen: DynamicCreditFacility},
	[Route.DISCOVERY_PAY]: {screen: DiscoveryPay}
};

export default APP_ROUTE_CONFIG;
