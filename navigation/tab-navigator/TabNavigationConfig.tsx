import { BottomTabNavigatorConfig } from 'react-navigation-tabs';

import Route from 'discovery-bank/navigation/Routes';

const TAB_NAVIGATOR_CONFIG: BottomTabNavigatorConfig = {
	initialRouteName: Route.TRANSFERS,
	backBehavior: 'initialRoute',
	tabBarComponent: () => null,
	defaultNavigationOptions: {
		tabBarVisible: false
	}
};

export default TAB_NAVIGATOR_CONFIG;
