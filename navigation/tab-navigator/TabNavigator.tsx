import { NavigationContainer } from 'react-navigation';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import APP_ROUTE_CONFIG from 'discovery-bank/navigation/tab-navigator/AppRoutesConfig';
import TAB_NAVIGATOR_CONFIG from 'discovery-bank/navigation/tab-navigator/TabNavigationConfig';

const TabNavigator: NavigationContainer = createBottomTabNavigator(APP_ROUTE_CONFIG, TAB_NAVIGATOR_CONFIG);

export default TabNavigator;
