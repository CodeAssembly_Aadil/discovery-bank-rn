import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';

import AUTH_ROUTE_CONFIG from 'discovery-bank/navigation/auth-flow/AuthRoutesConfig';
import Route from 'discovery-bank/navigation/Routes';

const AuthNavigator = createAnimatedSwitchNavigator(AUTH_ROUTE_CONFIG, {
	initialRouteName: Route.START
});

export default AuthNavigator;
