import { NavigationRouteConfigMap } from 'react-navigation';

import Application from 'discovery-bank/core/Application';
import AuthLoading from 'discovery-bank/features/auth/AuthLoading';
import SignIn from 'discovery-bank/features/auth/SignIn';
import Route from 'discovery-bank/navigation/Routes';

const AUTH_ROUTE_CONFIG: NavigationRouteConfigMap = {
	[Route.START]: {screen: AuthLoading},
	[Route.AUTH]: {screen: SignIn},
	[Route.APP]: {screen: Application}
};

export default AUTH_ROUTE_CONFIG;
