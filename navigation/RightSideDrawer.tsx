import React from 'react';

import { SafeAreaView, View } from 'react-native';

import { List, ListItem, Text } from 'native-base';

import NavigationService from 'discovery-bank/navigation/NavigationService';
import Route from 'discovery-bank/navigation/Routes';

const RightSideDrawer = () => {
	return (
		<View style={{flex: 1, backgroundColor: '#FFFFFF'}}>
			<SafeAreaView>
				<List>
					<ListItem button onPress={() => NavigationService.navigate(Route.DYNAMIC_CREDIT_FACILITY)}>
						<Text>Dynamic credit facility</Text>
					</ListItem>
					<ListItem
						button
						onPress={() => {
							NavigationService.navigate(Route.DISCOVERY_PAY);
						}}
					>
						<Text>Discovery pay</Text>
					</ListItem>
				</List>
			</SafeAreaView>
		</View>
	);
};

export default RightSideDrawer;
