import { ApplicationStateStore } from 'discovery-bank/core/ApplicationState.store';
import { NavigationActions, NavigationContainerComponent, NavigationStackAction, StackActions } from 'react-navigation';

export default class NavigationService {
	private static navigator: NavigationContainerComponent;
	private static rightDrawer: any;

	private static onNavigation(): void {
		NavigationService.closeRightDrawer();
		ApplicationStateStore.hideActionSheetButton();
		ApplicationStateStore.hideBackButton();
	}

	static setTopLevelNavigator(navigator: NavigationContainerComponent): void {
		NavigationService.navigator = navigator;
	}

	static setRightDrawer(drawer: any): void {
		NavigationService.rightDrawer = drawer;
	}

	static navigate(routeName: string, params: any = null): void {
		NavigationService.navigator.dispatch(
			NavigationActions.navigate({
				routeName,
				params
			})
		);
		NavigationService.onNavigation();
	}

	static navigateStack(action: NavigationStackAction): void {
		NavigationService.navigator.dispatch(action);
		NavigationService.onNavigation();
	}

	static pop(numScreens: number = 1): void {
		NavigationService.navigator.dispatch(
			StackActions.pop({
				n: numScreens
			})
		);
		NavigationService.onNavigation();
	}

	static popToTop(): void {
		NavigationService.navigator.dispatch(StackActions.popToTop());
		NavigationService.onNavigation();
	}

	static openRightDrawer(): void {
		NavigationService.rightDrawer._root.open();
	}

	static closeRightDrawer(): void {
		NavigationService.rightDrawer._root.close();
	}
}
