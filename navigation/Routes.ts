enum Route {
	// Top Level Routes
	START = 'Start',
	AUTH = 'Auth',
	APP = 'App',
	// Bottom Tab Routes
	HOME = 'Home',
	ACCOUNTS = 'Accounts',
	PAY = 'Pay',
	TRANSFERS = 'Transfers',
	MORE = 'More',
	// Drawer Routes
	DYNAMIC_CREDIT_FACILITY = 'Dynamic Credit Facility',
	DISCOVERY_PAY = 'Discovery Pay'
}

export default Route;
