import React, { Component } from 'react';

import { createAppContainer } from 'react-navigation';

import { StyleProvider } from 'native-base';
import getTheme from './native-base-theme/components';

import FontLoader from 'discovery-bank/core/assets/FontLoader';
import AuthNavigator from 'discovery-bank/navigation/auth-flow/AuthNavigator';
import NavigationService from 'discovery-bank/navigation/NavigationService';

const Navigator = createAppContainer(AuthNavigator);

export default class App extends Component<{}, {fontsLoaded: boolean}> {
	state = {fontsLoaded: false};

	async componentDidMount() {
		await FontLoader.loadAppFonts();
		this.setState({fontsLoaded: true});
	}

	render() {
		return (
			this.state.fontsLoaded && (
				<StyleProvider style={getTheme()}>
					<Navigator
						ref={(navigator) => {
							NavigationService.setTopLevelNavigator(navigator);
						}}
					/>
				</StyleProvider>
			)
		);
	}
}
