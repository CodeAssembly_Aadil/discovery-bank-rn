import { AccountCardType } from 'discovery-bank/core/constants/Account.enum';
import { CurrencySymbol } from 'discovery-bank/core/constants/Currency';

export default interface Account {
	name: string;
	number: string;
	productType: string;
	ccy: CurrencySymbol;
	balance: number;
	type: AccountCardType;
}
