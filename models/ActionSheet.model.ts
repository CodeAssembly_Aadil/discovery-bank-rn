export interface ActionSheetConfiguration {
	options: string[] | Array<{text: string; icon?: string; iconColor?: string}>;
	cancelButtonIndex?: number;
	destructiveButtonIndex?: number;
	title?: string;
}

export type ActionSheetHandler = (index: number) => void;
