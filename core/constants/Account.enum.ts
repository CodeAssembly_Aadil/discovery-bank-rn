export enum AccountCardType {
	BANK = 'Bank',
	BLACK = 'Black',
	GOLD = 'Gold',
	PLATINUM = 'Platinum',
	PURPLE = 'Purple',
	VITALITY = 'Vitality'
}

export const AccountCardImage: {[key in AccountCardType]: any} = {
	[AccountCardType.BANK]: require('../../assets/cards/Card_Bank.png'),
	[AccountCardType.BLACK]: require('../../assets/cards/Card_Black.png'),
	[AccountCardType.GOLD]: require('../../assets/cards/Card_Gold.png'),
	[AccountCardType.PLATINUM]: require('../../assets/cards/Card_Platinum.png'),
	[AccountCardType.PURPLE]: require('../../assets/cards/Card_Purple.png'),
	[AccountCardType.VITALITY]: require('../../assets/cards/Card_Vitality.png')
};
