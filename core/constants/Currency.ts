export enum CurrencyCode {
	ZAR = 'ZAR',
	USD = 'USD',
	GBP = 'GBP'
}

export enum CurrencySymbol {
	ZAR = 'R',
	USD = '$',
	GBP = '£'
}
