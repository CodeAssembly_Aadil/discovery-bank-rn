export default class CurrencyFormatter {
	private static separate(value: number, precision: number, placeholder: string, separator: string): string {
		if (!Number.isFinite(value)) {
			return placeholder ? placeholder : '';
		}

		const valueToPrecision: string = Math.abs(value).toFixed(precision);
		return valueToPrecision.replace(/\d{1,3}(?=(\d{3})+(?!\d))/g, `$&${separator}`);
	}

	static formatAsCurrency(
		value: number,
		precision: number = 2,
		placeholder: string = '—',
		separator: string = ', '
	): string {
		const seperated: string = CurrencyFormatter.separate(value, precision, placeholder, separator);
		const formattedValue: string = value < 0 ? `(${seperated})` : seperated;

		return formattedValue;
	}

	static formatAsCurrencyWithUnit(
		unit: string,
		value: number,
		precision: number = 2,
		placeholder: string = '—',
		separator: string = ', '
	): string {
		const seperated: string = CurrencyFormatter.separate(value, precision, placeholder, separator);
		const formattedValue: string = value < 0 ? `(${unit} ${seperated})` : `${unit} ${seperated}`;

		return formattedValue;
	}
}
