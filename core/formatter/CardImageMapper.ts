import { AccountCardImage, AccountCardType } from 'discovery-bank/core/constants/Account.enum';

export const getCardImageForAccoutType = (type: AccountCardType): any => {
	return type in AccountCardImage ? AccountCardImage[type] : AccountCardImage[AccountCardType.BANK];
};
