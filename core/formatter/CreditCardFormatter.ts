export const formatCreditCardNumber = (cardNumber: string | number): string => {
	if (!cardNumber) {
		return cardNumber as string;
	}

	// cardNumber.toString().
	return cardNumber.toString().replace(/(\d{4})/g, '$& ');
};
