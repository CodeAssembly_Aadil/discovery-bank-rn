import { action, observable } from 'mobx';

import { ActionSheetConfiguration, ActionSheetHandler } from 'discovery-bank/models/ActionSheet.model';

export class ApplicationState {
	@observable appHeaderTitle = 'Discovery Bank';

	@observable backButtonVisible = false;

	@observable actionSheetButtonVisible = false;
	@observable actionSheetConfiguration: ActionSheetConfiguration = null;
	@observable actionSheetHandler: ActionSheetHandler = null;

	@action setAppHeaderTitle = (value: string): void => {
		this.appHeaderTitle = value;
	}

	@action setActionSheetConfiguration = (value: ActionSheetConfiguration): void => {
		this.actionSheetConfiguration = value;
	}

	@action showBackButton(): void {
		this.backButtonVisible = true;
	}

	@action hideBackButton(): void {
		this.backButtonVisible = false;
	}

	@action showActionSheetButton(): void {
		this.actionSheetButtonVisible = true;
	}

	@action hideActionSheetButton(): void {
		this.actionSheetButtonVisible = false;
	}
}

export const ApplicationStateStore = new ApplicationState();
