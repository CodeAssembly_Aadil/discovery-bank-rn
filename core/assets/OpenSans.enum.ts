enum OpenSans {
	Light = 'OpenSans-Light',
	LightItalic = 'OpenSans-Light-Italic',
	Regular = 'OpenSans',
	Italic = 'OpenSans-Italic',
	Bold = 'OpenSans-Bold',
	BoldItalic = 'OpenSans-Bold-Italic'
}

export default OpenSans;
