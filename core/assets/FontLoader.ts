import * as Font from 'expo-font';

import OpenSans from 'discovery-bank/core/assets/OpenSans.enum';

export default class FontLoader {
	static async loadAppFonts() {
		await Font.loadAsync({
			[OpenSans.Light]: require('../../assets/fonts/OpenSans/OpenSans-Light.ttf'),
			[OpenSans.Regular]: require('../../assets/fonts/OpenSans/OpenSans.ttf'),
			[OpenSans.Bold]: require('../../assets/fonts/OpenSans/OpenSans-Bold.ttf'),
			// // Italics
			[OpenSans.LightItalic]: require('../../assets/fonts/OpenSans/OpenSans-Light-Italic.ttf'),
			[OpenSans.Italic]: require('../../assets/fonts/OpenSans/OpenSans-Italic.ttf'),
			[OpenSans.BoldItalic]: require('../../assets/fonts/OpenSans/OpenSans-Bold-Italic.ttf')
		});
	}
}
