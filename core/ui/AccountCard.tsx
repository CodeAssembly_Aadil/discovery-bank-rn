import CurrencyFormatter from 'discovery-bank/core/formatter/CurrencyFormatter';

import React from 'react';

import { Image, ImageStyle, StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';

import { getCardImageForAccoutType } from 'discovery-bank/core/formatter/CardImageMapper';
import { formatCreditCardNumber } from 'discovery-bank/core/formatter/CreditCardFormatter';
import Account from 'discovery-bank/models/Account.model';

export interface AccountCardProps {
	account: Account;
	style?: ViewStyle;
}

const styles = StyleSheet.create({
	cardImage: {
		flexBasis: 46,
		flexGrow: 1,
		flexShrink: 0,
		height: undefined,
		width: undefined,
		borderColor: 'lightgrey',
		borderWidth: 1,
		borderRadius: 2
	} as ImageStyle,

	details: {
		flex: 1,
		flexBasis: '100%',
		flexGrow: 0,
		flexShrink: 1,
		marginLeft: 12,
		justifyContent: 'space-between'
	} as ViewStyle,

	accountName: {fontSize: 14, marginBottom: 6, fontWeight: '700'} as TextStyle,
	accountNumber: {fontSize: 14, marginBottom: 16, color: 'darkgrey'} as TextStyle,

	detailsLastLine: {
		flexGrow: 1,
		flexDirection: 'row',
		alignContent: 'space-between',
		marginTop: 'auto'
	} as ViewStyle,

	productType: {fontSize: 14, marginRight: 'auto', color: 'darkgrey'} as TextStyle,
	accountBalance: {fontSize: 14, flex: 0} as TextStyle
});

const AccountCard = ({account, style = {}}: AccountCardProps) => (
	<View
		style={{
			flexDirection: 'row',
			paddingHorizontal: 6,
			paddingVertical: 8,
			backgroundColor: 'white',
			...style
		}}
	>
		<Image
			// source={require('../../assets/cards/Card_Black.png')}
			source={getCardImageForAccoutType(account.type)}
			style={styles.cardImage}
			resizeMode="contain"
		/>

		<View style={styles.details}>
			<Text ellipsizeMode="tail" numberOfLines={1} style={styles.accountName}>
				{account.name}
			</Text>

			<Text ellipsizeMode="tail" numberOfLines={1} style={styles.accountNumber}>
				{formatCreditCardNumber(account.number)}
			</Text>

			<View style={styles.detailsLastLine}>
				<Text ellipsizeMode="tail" numberOfLines={1} style={styles.productType}>
					{account.productType}
				</Text>
				<Text style={styles.accountBalance}>
					{CurrencyFormatter.formatAsCurrencyWithUnit(account.ccy, account.balance)}
				</Text>
			</View>
		</View>
	</View>
);

export default AccountCard;
