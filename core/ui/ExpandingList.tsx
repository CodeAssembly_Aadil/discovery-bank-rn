import React, { Component, ReactNode } from 'react';

import { View } from 'native-base';
import { FlatList, ListRenderItem, ViewStyle } from 'react-native';
import Collapsible from 'react-native-collapsible';
import Ripple from 'react-native-material-ripple';

export interface ExpandingListProps {
	placeholder: ReactNode;
	data: Array<any>;
	renderListItem: ListRenderItem<any>;
	keyExtractor?: (item: any, index?: number) => string;
	expandedListHeight: () => number | number | 'auto';
	onSelectItem: (itemData: any) => void;
	style?: ViewStyle;
}

interface ExpandingListState {
	isCollapsed: boolean;
}

export default class ExpandingList extends Component<ExpandingListProps, ExpandingListState> {
	constructor(props) {
		super(props);

		this.state = {
			isCollapsed: true
		};
	}

	static defaultProps: Partial<ExpandingListProps> = {
		style: {},
		keyExtractor: (item: any, index: number) => index.toString()
	};

	componentDidMount() {
		// console.log('ExpandingList', 'componentDidMount');
	}

	componentDidUpdate(prevProps: ExpandingListProps) {
		// console.log('ExpandingList', 'componentDidUpdate')
	}

	render() {
		return (
			<View
				style={{
					borderColor: 'lightgrey',
					borderWidth: 1,
					...this.props.style
				}}
			>
				{<Ripple onPress={this.toggle}>{this.props.placeholder}</Ripple>}
				<Collapsible collapsed={this.state.isCollapsed}>
					<FlatList
						style={{borderTopColor: 'lightgrey', borderTopWidth: 1, backgroundColor: 'lightgrey', height: 90 * 2.5}}
						data={this.props.data}
						renderItem={this.props.renderListItem}
						keyExtractor={this.props.keyExtractor}
					/>
				</Collapsible>
			</View>
		);
	}

	private toggle = () => {
		// console.log('toggle list', this.state.isCollapsed);
		this.setState({isCollapsed: !this.state.isCollapsed});
	}

	componentWillUnmount() {}
}
