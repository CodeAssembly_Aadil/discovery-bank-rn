import React from 'react';

import { LinearGradient } from 'expo-linear-gradient';

export const DiscoveryGradient = (props) => (
	<LinearGradient
		colors={[
			'rgb(30, 225, 203)',
			'rgb(99, 33, 254)',
			'rgb(255, 0, 153)',
		]}

		start={[0, 0]}
		end={[1, 0]}
		{...props}
	>
		{props.children}
	</LinearGradient>);
