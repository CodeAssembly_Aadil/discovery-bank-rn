import React, { Component } from 'react';

import { Container, Drawer, Footer, Root } from 'native-base';

import { NavigationRouter } from 'react-navigation';

import AppHeader from 'discovery-bank/core/app-header/AppHeader';
import NavigationService from 'discovery-bank/navigation/NavigationService';
import RightSideDrawer from 'discovery-bank/navigation/RightSideDrawer';
import BottomTabs from 'discovery-bank/navigation/tab-navigator/BottomTabs';
import TabNavigator from 'discovery-bank/navigation/tab-navigator/TabNavigator';

export default class Application extends Component {
	static router: NavigationRouter = TabNavigator.router;

	render() {
		return (
			<Root>
				<Container style={{flex: 1}}>
					<Drawer
						side="right"
						content={<RightSideDrawer />}
						ref={(drawer) => {
							NavigationService.setRightDrawer(drawer);
						}}
						onClose={() => {
							NavigationService.closeRightDrawer();
						}}
					>
						<AppHeader />
						<TabNavigator navigation={this.props.navigation} />
						<Footer style={{backgroundColor: 'white'}}>
							<BottomTabs></BottomTabs>
						</Footer>
					</Drawer>
				</Container>
			</Root>
		);
	}
}
