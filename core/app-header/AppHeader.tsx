import React from 'react';

import { SafeAreaView, StyleSheet, TextStyle, View, ViewStyle } from 'react-native';

import { ActionSheet, Title } from 'native-base';

import { Feather, Ionicons } from '@expo/vector-icons';

import Posed, { Transition } from 'react-native-pose';

import { observer } from 'mobx-react';

import { ApplicationStateStore } from 'discovery-bank/core/ApplicationState.store';
import { DiscoveryGradient } from 'discovery-bank/core/ui/DiscoveryGradient';
import { ActionSheetHandler } from 'discovery-bank/models/ActionSheet.model';
import NavigationService from 'discovery-bank/navigation/NavigationService';
import { PrimayColors } from 'discovery-bank/theme/Colors';
import Ripple from 'react-native-material-ripple';

const goBack = () => {
	NavigationService.pop();
};

const showActionSheet = () => {
	if (ApplicationStateStore.actionSheetConfiguration != null) {
		const onSelect: ActionSheetHandler = ApplicationStateStore.actionSheetHandler
			? ApplicationStateStore.actionSheetHandler
			: () => {};

		ActionSheet.show(ApplicationStateStore.actionSheetConfiguration, onSelect);
	}
};

const SIZE = 42;

const styles = StyleSheet.create({
	headerContent: {
		flexDirection: 'row',
		backgroundColor: PrimayColors.WHITE,
		alignContent: 'center',
		justifyContent: 'center',
		width: '100%'
	} as ViewStyle,

	actionContainer: {
		flex: 0,
		width: SIZE,
		height: SIZE
	} as ViewStyle,

	center: {
		flexShrink: 1,
		flexGrow: 0,
		flexBasis: '100%',
		justifyContent: 'center',
		alignItems: 'center',
		minHeight: SIZE
	} as ViewStyle,

	title: {
		flex: 0,
		fontSize: 16,
		lineHeight: 42,
		position: 'absolute',
		width: '100%',
		left: '-50%',
		top: -21
	} as TextStyle,

	actionButton: {
		flex: 0,
		width: SIZE,
		height: SIZE,
		borderRadius: 21,
		overflow: 'hidden'
	} as ViewStyle,

	icon: {
		width: SIZE,
		fontSize: 24,
		lineHeight: SIZE,
		textAlign: 'center'
	}
});

const AnimateInOutLeft = Posed.View({
	enter: {x: 0},
	exit: {x: -SIZE}
});

const AnimateInOutRight = Posed.View({
	enter: {x: 0},
	exit: {x: SIZE}
});

const AnimateInOutVertical = Posed.View({
	preEnter: {y: SIZE, opacity: 0},
	enter: {y: 0, opacity: 1},
	exit: {y: -SIZE, opacity: 0}
});

const AppHeader = observer(() => {
	const {appHeaderTitle, backButtonVisible, actionSheetButtonVisible} = ApplicationStateStore;
	return (
		<SafeAreaView>
			<View style={styles.headerContent}>
				<View style={styles.actionContainer}>
					<Transition animateOnMount>
						{backButtonVisible && (
							<AnimateInOutLeft key="animate-in-out-left">
								<Ripple onPress={goBack} style={styles.actionButton}>
									<Ionicons name="ios-arrow-back" size={24} style={styles.icon} />
								</Ripple>
							</AnimateInOutLeft>
						)}
					</Transition>
				</View>
				<View style={styles.center}>
					<Transition animateOnMount={true}>
						<AnimateInOutVertical key={appHeaderTitle}>
							<Title style={styles.title}>{appHeaderTitle}</Title>
						</AnimateInOutVertical>
					</Transition>
				</View>
				<View style={styles.actionContainer}>
					<Transition>
						{actionSheetButtonVisible && (
							<AnimateInOutRight key="animate-in-out-right">
								<Ripple onPress={showActionSheet} style={styles.actionButton}>
									<Feather name="more-vertical" size={24} style={styles.icon} />
								</Ripple>
							</AnimateInOutRight>
						)}
					</Transition>
				</View>
			</View>
			<DiscoveryGradient style={{height: 1}} />
		</SafeAreaView>
	);
});

export default AppHeader;
