module.exports = function(api) {
	api.cache(true);
	return {
		presets: ["babel-preset-expo"],
		plugins: [
			[
				"module-resolver",
				{
					alias: {
						"discovery-bank": "./"
					}
				}
			]
		],
		env: {
			production: {
				plugins: ["react-native-paper/babel"]
			}
		}
	};
};
