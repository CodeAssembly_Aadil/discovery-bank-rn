export enum PrimayColors {
	WHITE = '#FFFFFF',
	BLACK = '#000000',
	BLUE = '#114B8A',
	GOLD = '#B78A61',
	DARK_GRAY = '#292B2C',
	GRAY = '#737373',
	LIGHT_GRAY = '#EBEBEB'
}
